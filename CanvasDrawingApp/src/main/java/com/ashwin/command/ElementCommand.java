package com.ashwin.command;

/**
 * Marker Interface also extends Command Interface
 * Mark Element Commands
 *
 *	@author Ashwin Srivastava
 */ 

public interface ElementCommand extends Command{

}
