package com.ashwin.command;

import com.ashwin.canvas.elements.Point;
import com.ashwin.utils.ArgumentSanityCheck;
import com.ashwin.utils.CanvasUtility;

/**
 * Concrete Class
 * Creates instance of DrawBucketFillOnCanvasCommand (bucketFill)
 * Initializes instance variables.
 *
 *	@author Ashwin Srivastava
 */ 

public class DrawBucketFillOnCanvasCommand implements ElementCommand{

	private Point p;
	private String color;
	
	public DrawBucketFillOnCanvasCommand(String[] command_args) {
		// TODO Auto-generated constructor stub
		
		ArgumentSanityCheck.bucketFillSanityCheck(command_args);
		
		this.p = new Point(CanvasUtility.parseToInt(command_args[0]), CanvasUtility.parseToInt(command_args[1]));
		this.color = command_args[2];
		
	}

	public Point getP() {
		return p;
	}

	public void setP(Point p) {
		this.p = p;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

}
