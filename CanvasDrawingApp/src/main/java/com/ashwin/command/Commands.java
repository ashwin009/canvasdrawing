package com.ashwin.command;

/**
 * List of executable commands, 
 * within the scope of the Problem statement
 *
 *	@author Ashwin Srivastava
 */ 

public enum Commands {

	C,
	L,
	R,
	B,
	Q;
	
}