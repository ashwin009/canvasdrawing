package com.ashwin.command;

import com.ashwin.canvas.elements.Point;
import com.ashwin.utils.ArgumentSanityCheck;
import com.ashwin.utils.CanvasUtility;

/**
 * Concrete Class
 * Creates instance of DrawRectangleOnCanvasCommand (draws rectangle on the Canvas)
 * Initializes instance variables.
 *
 *	@author Ashwin Srivastava
 */ 


public class DrawRectangleOnCanvasCommand implements ElementCommand{
	
	private Point p1;
	private Point p2;

	public DrawRectangleOnCanvasCommand(String[] command_args) {
		// TODO Auto-generated constructor stub
		
		ArgumentSanityCheck.rectangleSanityCheck(command_args.length);
		
		this.p1 = new Point(CanvasUtility.parseToInt(command_args[0]), CanvasUtility.parseToInt(command_args[1]));
		this.p2 = new Point(CanvasUtility.parseToInt(command_args[2]), CanvasUtility.parseToInt(command_args[3]));
		
	}
	
	public Point getP1() {
		return p1;
	}

	public void setP1(Point p1) {
		this.p1 = p1;
	}

	public Point getP2() {
		return p2;
	}

	public void setP2(Point p2) {
		this.p2 = p2;
	}

}
