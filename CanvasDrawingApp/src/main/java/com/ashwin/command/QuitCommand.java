package com.ashwin.command;

import java.util.Scanner;

/**
 * Executes quit command by calling System.exit(0)
 *
 *	@author Ashwin Srivastava
 */ 


public class QuitCommand implements Command {

	public static void quit(Scanner scanner) {
		// TODO Auto-generated method stub
		scanner.close();
		System.out.println("Quitting Canvas Drawing App");
		System.exit(0);
	}

}
