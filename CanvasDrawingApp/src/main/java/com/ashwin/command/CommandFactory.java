
package com.ashwin.command;

import java.util.Arrays;

import com.ashwin.exception.IllegalCommandArgumentException;
import com.ashwin.exception.IllegalCommandException;

/**
 * Factory Pattern Implementation
 * Initializes instance of the specific element command
 * based on the user input
 *
 *	@author Ashwin Srivastava
 */ 

public class CommandFactory {

	public Command getCommand(String commandToExecute) throws IllegalCommandException, IllegalCommandArgumentException{
		// TODO Auto-generated method stub
		
		String[] input_args = commandToExecute.split(" ");
		String command = input_args[0].toUpperCase();
		String[] command_args = Arrays.copyOfRange(input_args, 1, input_args.length);

		if(command.equals(Commands.C.name())) {
			return new DrawCanvasCommand(command_args);
		}else if(command.equals(Commands.L.name())) {
			return new DrawLineOnCanvasCommand(command_args);
		}else if(command.equals(Commands.R.name())) {
			return new DrawRectangleOnCanvasCommand(command_args);
		}else if(command.equals(Commands.B.name())) {
			return new DrawBucketFillOnCanvasCommand(command_args);
		}else if(command.equals(Commands.Q.name())) {
			return new QuitCommand();
		}else {
			throw new IllegalCommandException();
		}
	}

}
