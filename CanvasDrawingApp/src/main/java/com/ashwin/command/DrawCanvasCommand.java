package com.ashwin.command;

import com.ashwin.utils.ArgumentSanityCheck;
import com.ashwin.utils.CanvasUtility;

/**
 * Concrete Class
 * Creates instance of DrawCanvasCommand (draws emptyCanvas)
 * Initializes instance variables.
 *
 *	@author Ashwin Srivastava
 */ 


public class DrawCanvasCommand implements  Command {

	private int width;
	private int height;
	
	public DrawCanvasCommand(String[] command_args) {
		// TODO Auto-generated constructor stub
		
		ArgumentSanityCheck.canvasSanityCheck(command_args.length);
		
		this.width = CanvasUtility.parseToInt(command_args[0]); 
		this.height = CanvasUtility.parseToInt(command_args[1]);
		
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}
	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

}
