package com.ashwin.utils;

import com.ashwin.canvas.elements.Point;
import com.ashwin.exception.IllegalCommandArgumentException;

/**
 * This Utility class does Sanity check on the input data
 * 
 *	@author Ashwin Srivastava
 */

public class ArgumentSanityCheck {

	public static void inFirstQuadrant(Point...points) {
		// TODO Auto-generated method stub
		for(Point point: points) {
			pointInFirstQuadrant(point);
		}
	}
	
	public static void pointInFirstQuadrant(Point p) {
		if(p.getX() < 1 || p.getY() < 1) {
			throw new IllegalArgumentException("Point should be in the First Quadrant -> Coordinates should be greater than ZERO (>0)");
		}
	}
	
	public static void isOriginOrInFirstQuadrant(int x, int y) {
		if(x < 0 || y < 0) {
			throw new IllegalArgumentException("Point should be in the First Quadrant or can be Origin(0,0) -> Coordinates should be greater than or equal to ZERO(>=0)");
		}
	}
	
	public static boolean isOrigin(Point p) {
		if(p.getX() == 0 && p.getY() == 0) {
			return true;
		}else {
			return false;
		}
			
	}

	public static boolean isLine(Point p1, Point p2) {
		// TODO Auto-generated method stub
		if(p1.getX() == p2.getX() || p1.getY() == p2.getY()) {
			return true;
		}else {
			return false;
		}
		
	}

	public static void isNonZeroPositive(int...coordinates) {
		// TODO Auto-generated method stub
		for(int c: coordinates) {
			if(c < 1) {
				throw new IllegalArgumentException("Coordinate should be greater than ZERO(>0)");
			}
		}
	}
	
	public static void isNonNegative(int...coordinates) {
		// TODO Auto-generated method stub
		for(int c: coordinates) {
			if(c < 0) {
				throw new IllegalArgumentException("Coordinate should be greater than or equal to ZERO(>=0)");
			}
		}
	}
	
	public static void lineSanityCheck(int length) {
		if(length < 4) {
			throw new IllegalCommandArgumentException("To draw a LINE we need atleast 4 points", CanvasConstants.LineCommandSyntax);
		}else if(length > 4) {
			System.out.println("Only first 4 coordinates will be considered to draw the LINE");
		}
	}
	
	public static void rectangleSanityCheck(int length) {
		if(length < 4) {
			throw new IllegalCommandArgumentException("To draw a RECTANGLE we need atleast 4 points", CanvasConstants.RectangleCommandSyntax);
		}else if(length > 4) {
			System.out.println("Only first 4 coordinates will be considered to draw the RECTANGLE");
		}
	}

	public static void bucketFillSanityCheck(String[] command_args) {
		if(command_args.length < 3) {
			throw new IllegalCommandArgumentException("To BUCKET FILL we need atleast 3 points", CanvasConstants.BucketFillCommandSyntax);
		}else if(command_args.length > 3) {
			System.out.println("Only first 3 arguments will be considered for BUCKET FILL");
		}
		
		if(command_args[2].length() != 1) {
			throw new IllegalCommandArgumentException("Size of color token should be ONE", CanvasConstants.BucketFillCommandSyntax);
		}
	}
	
	public static void canvasSanityCheck(int length) {
		if(length < 2) {
			throw new IllegalCommandArgumentException("To draw a CANVAS we need atleast 2 arguments", CanvasConstants.NewCanvasSyntax);
		}else if(length > 2) {
			System.out.println("Only first 2 coordinates will be considered to draw NEW CANVAS");
		}
	}
	

}
