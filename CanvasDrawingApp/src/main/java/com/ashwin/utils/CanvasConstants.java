package com.ashwin.utils;

/**
 * 
 * This class defines various constants needed
 * All constants can be defined here
 *
 *	@author Ashwin Srivastava
 */

public class CanvasConstants {

	public static final String horizontalEdge = "-";
	public static final String verticalEdge = "|";
	public static final String lineBuilder = "x";
	
	public static final String NewCanvasSyntax = "C w h -> Create a new canvas of width w and height h.";
	
	public static final String LineCommandSyntax = "L x1, y1, x2, y2 -> Draw a new line from (x1,y1) to (x2,y2). "
														+ "Currently, only horizontal or vertical lines are supported. "
														+ "Horizontal and vertical lines will be drawn using the 'x' character.";
	
	public static final String RectangleCommandSyntax = "R x1, y1, x2, y2 -> Draw a rectangle whose upper left corner is (x1,y1) and lower right corner is (x2,y2). "
														+ "Horizontal and vertical lines will be drawn using the 'x' character";
	
	public static final String BucketFillCommandSyntax = "B x y c -> Fill the entire area connected to (x,y) with \"colour\" c. "
														+ "The behaviour of this is the same as that of the \"bucket fill\" tool in paint programs.";
	
	public static final String QuitCommandSyntax = "Q -> Quit";
	
	public static final String CoordinateNonZeroPositive = "Coordinate should be greater than ZERO. "
														+ "Only NON ZERO POSITIVE INTEGER is Accepted ";
}
