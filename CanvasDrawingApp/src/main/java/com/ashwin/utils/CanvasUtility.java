package com.ashwin.utils;

import java.util.Arrays;

import com.ashwin.canvas.elements.Point;
import com.ashwin.exception.IllegalCanvasElementException;
import com.ashwin.exception.IllegalCommandArgumentException;

/**
 * This Utility class provides static methods that helps to
 * draw elements on the Canvas
 *
 *	@author Ashwin Srivastava
 */

public class CanvasUtility {
	
	public static int parseToInt(String in) {
		int coordinate = Integer.parseInt(in);
		if(coordinate <=0) {
			throw new IllegalCommandArgumentException("input Error", CanvasConstants.CoordinateNonZeroPositive);
		}
		
		return coordinate;
	}

	public static String createHorizontalLine(int width) {
		// TODO Auto-generated method stub
		StringBuilder sb = new StringBuilder();
		for(int i=0; i<width+2; i++) {
			sb.append(CanvasConstants.horizontalEdge);
		}
		return sb.toString();
	}

	public static void fillArray(String[][] elementCache) {
		// TODO Auto-generated method stub
		for(String[] s: elementCache) {
			Arrays.fill(s, " ");
		}
	}
	
	public static void isOutOfCanvas(Point p, int width, int height, String element) {
		if(p.getX() < 1 || p.getX() >= width ||
				p.getY() < 1 || p.getY() >= height) {
			throw new IllegalCanvasElementException(element+ " is out of Canvas");
		}
	}

	public static void diagonalCheck(Point p1, Point p2) {
		// TODO Auto-generated method stub
		if(p1.getX() != p2.getX() && p1.getY() != p2.getY()) {
			throw new IllegalArgumentException("Currently, only Horizontal and Vertical lines are supported.");
		}
	}

	public static boolean isVerticalLine(int x1, int x2) {
		// TODO Auto-generated method stub
		return (x1 == x2) ? true : false;
	}

	public static boolean isHorizontalLine(int y1, int y2) {
		// TODO Auto-generated method stub
		return (y1 == y2) ? true : false;
	}

}
