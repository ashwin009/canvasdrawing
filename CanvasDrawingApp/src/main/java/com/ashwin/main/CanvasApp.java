package com.ashwin.main;

import java.util.Scanner;

import com.ashwin.canvas.Canvas;
import com.ashwin.canvas.CanvasRendering;
import com.ashwin.canvas.element.CanvasElementFactory;
import com.ashwin.command.Command;
import com.ashwin.command.CommandFactory;
import com.ashwin.command.DrawCanvasCommand;
import com.ashwin.command.ElementCommand;
import com.ashwin.command.QuitCommand;
import com.ashwin.exception.IllegalCanvasElementException;
import com.ashwin.exception.IllegalCommandArgumentException;
import com.ashwin.exception.IllegalCommandException;

/**
 * CanvasApp is the Entry point.
 * 
 * It initializes various Factory class instances
 * Scanner helps to get the User Input through Console
 * 
 * Command can be of 3 major categories
 * - Draw Empty Canvas (first and mandatory step)
 * - Draw Element on the Canvas
 * - Quit
 *
 *	@author Ashwin Srivastava
 */
public class CanvasApp 
{
	
	private static void initialize() {
		// TODO Auto-generated method stub
		scanner = new Scanner(System.in);
		commandFactory = new CommandFactory();
		canvasElementFactory = new CanvasElementFactory();
	}
	
    private static void executeCommand(String commandToExecute) {
    	
    		Command command = null;
    		
    		try {
    			command = commandFactory.getCommand(commandToExecute);
    		}catch(IllegalCommandException ice) {
    			System.out.println("Please enter a Valid Command");
    		}catch(IllegalCommandArgumentException icae) {
    			System.out.println("Command Syntax Error: " + icae.getMessage());
    			System.out.println("Sample Command with Correct Syntax: " + icae.getCommandInstruction());
    		}
    		
    		if(command instanceof QuitCommand) {
    			QuitCommand.quit(scanner);
    		}
    		
    		if(command instanceof DrawCanvasCommand) {
    			drawNewCanvas((DrawCanvasCommand) command);
    			return;
    		}
    		
    		if(command instanceof ElementCommand) {
    			drawElement((ElementCommand) command);
    		}
    		
    		
    }

	private static void drawElement(ElementCommand command) {
		// TODO Auto-generated method stub
		if(canvas == null) {
			throw new IllegalStateException("Before drawing any element, Canvas need to be there, so draw an Empty Canvas first.");
			
		}
		
		try {
			canvas.addCanvasElement(canvasElementFactory.getCanvasElement(command));
			System.out.println(canvas.drawCanvas());
		}catch(IllegalCanvasElementException icee) {
			System.out.println("Exception occured while adding Element to the Canvas");
			System.out.println(icee.getMessage());
		}
	}

	private static void drawNewCanvas(DrawCanvasCommand command) {
		// TODO Auto-generated method stub
		canvas = new CanvasRendering(command.getWidth(), command.getHeight());
		System.out.println(canvas.drawCanvas());
	}

	private static Scanner scanner;
    private static Canvas canvas;
    private static CommandFactory commandFactory;
    private static CanvasElementFactory canvasElementFactory;
    
    public static void main( String[] args ) {

		System.out.println( "Canvas Drawing App" );
    
		initialize();
    
		System.out.println("First Step will be draw an Empty Canvas");
		System.out.println("enter command: ");
    
		while(true) {
    			executeCommand(scanner.nextLine());
    			System.out.println("enter command: ");
		}
    }

	
}
