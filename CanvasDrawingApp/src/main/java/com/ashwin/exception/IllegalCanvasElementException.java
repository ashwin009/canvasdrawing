package com.ashwin.exception;

/**
 * This Runtime Exception catches Illegal Elements
 * Currently we can draw below Elements only
 * - Draw a LINE on the Canvas
 * - Draw a RECTANGLE on the Canvas
 * - Bucket Fill on the Canvas
 * 
 * for any other element this Exception is thrown
 * 
 *	@author Ashwin Srivastava
 */

public class IllegalCanvasElementException extends RuntimeException{

	public IllegalCanvasElementException(String msg) {
		// TODO Auto-generated constructor stub
		super(msg);
	}

}
