package com.ashwin.exception;

/**
 *  This Runtime Exception catches Illegal Elements
 *  
 *  @param commandInstruction
 *  Problem statement mentions detailed command Instruction
 *  
 *	@author Ashwin Srivastava
 */

public class IllegalCommandArgumentException extends RuntimeException {
	
	String commandInstruction;

	public IllegalCommandArgumentException(String message, String commandsyntax) {
		// TODO Auto-generated constructor stub
		super(message);
		this.commandInstruction = commandsyntax;
	}

	public String getCommandInstruction() {
		// TODO Auto-generated method stub
		return this.commandInstruction;
	}

}
