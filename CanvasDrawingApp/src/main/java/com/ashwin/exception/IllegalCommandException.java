package com.ashwin.exception;

/**
 * This Runtime Exception catches Illegal Command
 * Currently we can execute below commands only
 * - Create an Empty Canvas
 * - Draw an Element on the Canvas
 * 
 * All these commands are defined in Enum 
 * @see Commands
 * 
 * IllegalCanvasElementException catches the Element Exceptions
 * 
 * for any other element this Exception is thrown
 * 
 *	@author Ashwin Srivastava
 */

public class IllegalCommandException extends RuntimeException {

}
