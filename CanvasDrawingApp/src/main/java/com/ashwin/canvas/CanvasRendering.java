package com.ashwin.canvas;

import java.util.LinkedList;
import java.util.Stack;

import com.ashwin.canvas.element.CanvasElement;
import com.ashwin.canvas.elements.BucketFill;
import com.ashwin.canvas.elements.Line;
import com.ashwin.canvas.elements.Point;
import com.ashwin.canvas.elements.Rectangle;
import com.ashwin.exception.IllegalCanvasElementException;
import com.ashwin.utils.CanvasConstants;
import com.ashwin.utils.CanvasUtility;

/**
 * Renders the drawing on the Canvas
 * add Element(generic) method adds element to specific element type
 * which eventually calls draw method of that element
 * 
 * @author Ashwin Srivastava
 */

public class CanvasRendering implements Canvas {
	
	private final String[][] elementCache;
	
	private final int width;
	private final int height;
	private final String horizontalLine;
	
	private LinkedList<CanvasElement> elements;
	
	public CanvasRendering(int w, int h) {
		// TODO Auto-generated constructor stub
		
		this.width = w;
		this.height = h;
		
		elements =  new LinkedList();
		horizontalLine = CanvasUtility.createHorizontalLine(this.width);
		elementCache = new String[this.height][this.width];
		CanvasUtility.fillArray(elementCache);
				
	}
	
	public String drawCanvas() {
		// TODO Auto-generated method stub
		StringBuilder sb = new StringBuilder();
		
		sb.append(horizontalLine).append("\n");
		
		for(int i=0; i<this.height; i++) {
			sb.append(CanvasConstants.verticalEdge);
			for(int j=0; j<this.width; j++) {
				sb.append(elementCache[i][j]);
			}
			
			sb.append(CanvasConstants.verticalEdge);
			sb.append("\n");
		}
		
		sb.append(horizontalLine);
		
		return sb.toString();
	}

	public void addCanvasElement(CanvasElement canvasElement) throws IllegalCanvasElementException {
		// TODO Auto-generated method stub
		elements.add(canvasElement);
		if(canvasElement instanceof Line) {
			addLineToCanvas((Line) canvasElement);
		}else if(canvasElement instanceof Rectangle) {
			addRectangleToCanvas((Rectangle) canvasElement);
		}else if(canvasElement instanceof BucketFill) {
			addBucketFillToCanvas((BucketFill) canvasElement);
		}
	}

	private void addBucketFillToCanvas(BucketFill bucketFill) {
		// TODO Auto-generated method stub
		CanvasUtility.isOutOfCanvas(bucketFill.getP(), this.width, this.height, "Bucket Fill");
		
		bucketFillOnCanvas(bucketFill.getP(), bucketFill.getColor());
	
	}

	private void bucketFillOnCanvas(Point p, String color) {
		// TODO Auto-generated method stub
		int x = p.getX(); int y = p.getY();
		
		String initialCache = elementCache[y-1][x-1];
		Stack<Point> bucket = new Stack();
		bucket.add(new Point(y-1, x-1));
		
		while(!bucket.isEmpty()) {
			
			Point point = bucket.pop();
			if(elementCache[point.getX()][point.getY()] == initialCache) {
				elementCache[point.getX()][point.getY()] = color;
			}
			
			if(point.getX() - 1 >= 0 && 
					elementCache[point.getX()-1][point.getY()] == initialCache) {
				bucket.add(new Point(point.getX()-1, point.getY()));
			}
			
			if(point.getX() + 1 < height && 
					elementCache[point.getX()+1][point.getY()] == initialCache) {
				bucket.add(new Point(point.getX()+1, point.getY()));
			}
			
			if(point.getY() - 1 >= 0 && 
					elementCache[point.getX()][point.getY()-1] == initialCache) {
				bucket.add(new Point(point.getX(), point.getY()-1));
			}
			
			if(point.getY() + 1 < width && 
					elementCache[point.getX()][point.getY()+1] == initialCache) {
				bucket.add(new Point(point.getX(), point.getY()+1));
			}
		}
		
		
	}

	private void addRectangleToCanvas(Rectangle rectangle) {
		// TODO Auto-generated method stub
		CanvasUtility.isOutOfCanvas(rectangle.getP1(), this.width, this.height, "Rectanle");
		
		drawRectangleOnCanvas(rectangle.getP1(), rectangle.getP2(), rectangle.isLine());
	}

	private void drawRectangleOnCanvas(Point p1, Point p2, boolean isLine) {
		// TODO Auto-generated method stub
		
		int x1 = p1.getX(); int y1 = p1.getY();
		int x2 = p2.getX(); int y2 = p2.getY();
		
		if(isLine) {
			drawLineOnCanvas(p1, p2);
		}else {
			drawLine(x1, y1, x2, y1);
			drawLine(x1, y1, x1, y2);
			drawLine(x2, y1, x2, y2);
			drawLine(x1, y2, x2, y2);
		}
		
	}
	
	private void addLineToCanvas(Line line) {
		// TODO Auto-generated method stub
		CanvasUtility.isOutOfCanvas(line.getP1(), this.width, this.height, "Line");
		
		drawLineOnCanvas(line.getP1(), line.getP2());
	}

	private void drawLineOnCanvas(Point p1, Point p2) {
		// TODO Auto-generated method stub
		int x1 = p1.getX(); int y1 = p1.getY();
		int x2 = p2.getX(); int y2 = p2.getY();
		
		drawLine(x1, y1, x2, y2);
	}

	private void drawLine(int x1, int y1, int x2, int y2) {
		// TODO Auto-generated method stub
		for(int row = y1-1; row <= y2-1 && row < height; row++) {
			for(int col = x1-1; col <= x2-1 && col < width; col++) {
				elementCache[row][col] = CanvasConstants.lineBuilder;
			}
		}
		
	}

	

}
