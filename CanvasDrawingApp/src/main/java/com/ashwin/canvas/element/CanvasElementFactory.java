package com.ashwin.canvas.element;

import com.ashwin.canvas.elements.BucketFill;
import com.ashwin.canvas.elements.Line;
import com.ashwin.canvas.elements.Rectangle;
import com.ashwin.command.DrawBucketFillOnCanvasCommand;
import com.ashwin.command.DrawLineOnCanvasCommand;
import com.ashwin.command.DrawRectangleOnCanvasCommand;
import com.ashwin.command.ElementCommand;

/**
 * Factory Pattern Implementation
 * Initializes instance of the Specific element
 * 
 * @author Ashwin Srivastava
 */

public class CanvasElementFactory {

	public CanvasElement getCanvasElement(ElementCommand command) {
		// TODO Auto-generated method stub
		if(command instanceof DrawLineOnCanvasCommand) {
			
			DrawLineOnCanvasCommand cmd = (DrawLineOnCanvasCommand) command;
			return new Line(cmd.getP1(), cmd.getP2());
			
		}else if(command instanceof DrawRectangleOnCanvasCommand) {
			
			DrawRectangleOnCanvasCommand cmd = (DrawRectangleOnCanvasCommand) command;
			return new Rectangle(cmd.getP1(), cmd.getP2());
			
		}else if(command instanceof DrawBucketFillOnCanvasCommand) {
			
			DrawBucketFillOnCanvasCommand cmd = (DrawBucketFillOnCanvasCommand) command;
			return new BucketFill(cmd.getP(), cmd.getColor());
			
		}else {
			
			return null;
		}
	}

}
