package com.ashwin.canvas;

/**
 * The Sktechboard where different Canvas Elements 
 * can be drawn. This interface provide its users
 * precise control over where to draw a canvas element.
 * Also it helps to draw the Empty Canvas which
 * happens to be the starting point as well.
 * 
 * @author Ashwin Srivastava
 */
import com.ashwin.canvas.element.CanvasElement;
import com.ashwin.exception.IllegalCanvasElementException;

public interface Canvas {

	String drawCanvas();
	
	void addCanvasElement(CanvasElement canvasElement) throws IllegalCanvasElementException;

}
