package com.ashwin.canvas.elements;

/**
 *  Point Utility Class
 *
 * 	@overrides equals(Object o) and hashcode() methods
 * 
 *	@author Ashwin Srivastava
 */

public class Point {
	
	private int x;
	private int y;
	
	public Point(int x, int y) {
		// TODO Auto-generated constructor stub
		this.x = x;
		this.y = y;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + x;
		result = prime * result + y;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		
		Point p = (Point) obj;
		
		if(this.x != p.x) {
			return false;
		}
		return this.y == p.y;
		
	}
	
	

}
