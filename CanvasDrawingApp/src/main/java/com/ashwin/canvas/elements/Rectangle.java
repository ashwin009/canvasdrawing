package com.ashwin.canvas.elements;

import com.ashwin.canvas.element.CanvasElement;
import com.ashwin.utils.ArgumentSanityCheck;

/**
 * Rectangle Element
 * Implements CanvasElement
 * Constructor does sanity check as well as initializes Rectangle class instance
 * 
 *
 *	@author Ashwin Srivastava
 */

public class Rectangle implements CanvasElement{
	
	private boolean isLine = false;
	private Point p1;
	private Point p2;
	
	public Rectangle(Point p1, Point p2) {
		// TODO Auto-generated constructor stub
		
		ArgumentSanityCheck.inFirstQuadrant(p1, p2);
		isLine = ArgumentSanityCheck.isLine(p1, p2);
		
		this.p1 = p1;
		this.p2 = p2;
		
	}

	public boolean isLine() {
		return isLine;
	}

	public Point getP1() {
		return p1;
	}

	public void setP1(Point p1) {
		this.p1 = p1;
	}

	public Point getP2() {
		return p2;
	}

	public void setP2(Point p2) {
		this.p2 = p2;
	}

}
