package com.ashwin.canvas.elements;

import com.ashwin.canvas.element.CanvasElement;
import com.ashwin.utils.ArgumentSanityCheck;
import com.ashwin.utils.CanvasUtility;

/**
 * Line Element
 * Implements CanvasElement
 * Constructor does sanity check as well as initializes Line class instance
 * 
 * @author Ashwin Srivastava
 */

public class Line implements CanvasElement{
	
	private Point p1;
	private Point p2;
	
	public Line(Point p1, Point p2) {
		// TODO Auto-generated constructor stub
		
		ArgumentSanityCheck.inFirstQuadrant(p1, p2);
		
		CanvasUtility.diagonalCheck(p1, p2);
		
		this.p1 = p1;
		this.p2 = p2;
		
		int x1 = this.p1.getX(); int y1 = this.p1.getY();
		int x2 = this.p2.getX(); int y2 = this.p2.getY();
		
		boolean verticalLine = CanvasUtility.isVerticalLine(x1, x2);
		boolean horizontalLine = CanvasUtility.isHorizontalLine(y1, y2);
		
		if(verticalLine || horizontalLine) {
			
			this.p1.setX(Math.min(x1, x2));
			this.p2.setX(Math.max(x1, x2));
			
			this.p1.setY(Math.min(y1, y2));
			this.p2.setY(Math.max(y1, y2));
		}
		
	}

	public Point getP1() {
		return p1;
	}

	public void setP1(Point p1) {
		this.p1 = p1;
	}

	public Point getP2() {
		return p2;
	}

	public void setP2(Point p2) {
		this.p2 = p2;
	}

}
