package com.ashwin.canvas.elements;

import com.ashwin.canvas.element.CanvasElement;
import com.ashwin.utils.ArgumentSanityCheck;

/**
 * BucketFill Element
 * Implements CanvasElement
 * Constructor does sanity check as well as initializes BucketFill class instance
 * 
 * @author Ashwin Srivastava
 */


public class BucketFill implements CanvasElement {
	
	private Point p;
	private String color;
	
	public BucketFill(Point p, String color) {
		// TODO Auto-generated constructor stub
		ArgumentSanityCheck.isNonZeroPositive(p.getX(), p.getY());
		this.p = p;
		this.color = color;
	}

	public Point getP() {
		return p;
	}

	public void setP(Point p) {
		this.p = p;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

}
